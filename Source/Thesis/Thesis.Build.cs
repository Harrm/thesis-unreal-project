// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Thesis : ModuleRules
{
	public Thesis(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
		CppStandard = CppStandardVersion.Latest;

		PublicDependencyModuleNames.AddRange(new string[]
		{
			"Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "NavigationSystem", "AIModule",
			"Blockchain"
		});
	}
}
