// Copyright Epic Games, Inc. All Rights Reserved.

#include "Thesis/Thesis.h"

#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Thesis, "Thesis" );

DEFINE_LOG_CATEGORY(LogThesis)
 