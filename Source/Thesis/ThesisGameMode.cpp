// Copyright Epic Games, Inc. All Rights Reserved.

#include "Thesis/ThesisGameMode.h"
#include "Thesis/ThesisPlayerController.h"
#include "Thesis/ThesisCharacter.h"

#include "UObject/ConstructorHelpers.h"
#include "GameFramework/HUD.h"
#include "GameFramework/SpectatorPawn.h"

AThesisGameMode::AThesisGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AThesisPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDownCPP/Blueprints/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = ASpectatorPawn::StaticClass();
	}

	static ConstructorHelpers::FClassFinder<AHUD> HudBPClass(TEXT("/Game/UI/HUD_Player"));
	if (HudBPClass.Class != nullptr)
	{
		HUDClass = HudBPClass.Class;
	}
}
