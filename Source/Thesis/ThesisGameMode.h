// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ThesisGameMode.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FNewTurnDelegate);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FTurnOverDelegate);

UCLASS(minimalapi)
class AThesisGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AThesisGameMode();

	virtual void OnTurnStart()
	{
		
	}

	virtual void OnTurnOver()
	{
		
	}

private:
};



